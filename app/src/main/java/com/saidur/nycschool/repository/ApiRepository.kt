package com.saidur.nycschool.repository

import com.saidur.nycschool.api.SchoolWebDataApi
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class ApiRepository @Inject constructor(
    private val apiServices: SchoolWebDataApi
) {
    suspend fun getSatScoreDetail(dbn: String) = apiServices.getSatScoreDetail(dbn)
    suspend fun getSchoolList() = apiServices.getSchoolList()
}