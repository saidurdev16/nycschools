package com.saidur.nycschool.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.saidur.nycschool.model.BaseResponse
import com.saidur.nycschool.model.SchoolSatScore
import com.saidur.nycschool.repository.ApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolInfoAndSATScoreActivityViewModel @Inject constructor(private val repository: ApiRepository) :
    ViewModel() {

    val satScoreResult: MutableLiveData<BaseResponse<List<SchoolSatScore>>> =
        MutableLiveData()

    fun getSATScore(schoolDBNNumber: String) {
        satScoreResult.value = BaseResponse.Loading()
        viewModelScope.launch {
            try {
                val response = repository.getSatScoreDetail(schoolDBNNumber)
                if (response.code() == 200) {
                    satScoreResult.value = BaseResponse.Success(response.body())
                } else {
                    satScoreResult.value = BaseResponse.Error(response.message())
                }

            } catch (ex: Exception) {
                satScoreResult.value = BaseResponse.Error(ex.message)
            }
        }
    }

}