package com.saidur.nycschool.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.saidur.nycschool.databinding.SchoolSatDetailsViewBinding
import com.saidur.nycschool.model.BaseResponse
import com.saidur.nycschool.model.SchoolInfo
import com.saidur.nycschool.ui.viewmodels.SchoolInfoAndSATScoreActivityViewModel
import com.saidur.nycschool.utils.Constants.SCHOOL_INFO
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolInfoAndSATScoreActivity : AppCompatActivity() {
    private lateinit var viewModel: SchoolInfoAndSATScoreActivityViewModel
    private lateinit var binding: SchoolSatDetailsViewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SchoolSatDetailsViewBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // obtain view-model instance
        viewModel = ViewModelProvider(this)[SchoolInfoAndSATScoreActivityViewModel::class.java]

        // this creates a vertical layout Manager
        // fetch school data from server
        val schoolInfo = intent.getSerializableExtra(SCHOOL_INFO) as? SchoolInfo
        if (schoolInfo != null) {
            binding.schoolDetails = schoolInfo
            viewModel.getSATScore(schoolInfo.dbn)
        }
        // observe server response
        observeSchoolDataResult()

        binding.closeButton.setOnClickListener { finish() }

        binding.mapButton.setOnClickListener {
            schoolInfo?.apply {
                val uri = (((((("geo:"
                        + latitude) + ","
                        + longitude) + "?q="
                        + schoolName) + ", "
                        + primaryAddressLine1) + ", "
                        + city) + ", "
                        + stateCode)
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            }
        }

        binding.phoneButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + schoolInfo?.phoneNumber)
            startActivity(intent)
        }

        binding.emailButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "plain/text"
            intent.putExtra(
                Intent.EXTRA_EMAIL,
                arrayOf(schoolInfo?.schoolEmail.toString())
            )
            startActivity(intent)
        }

        binding.websiteButton.setOnClickListener {
            var website = ""
            if (schoolInfo != null) {
                website = if (schoolInfo.website.contains("http")) {
                    schoolInfo.website
                } else {
                    "https://" + schoolInfo.website
                }
            }
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(website))
            startActivity(intent)
        }

    }

    private fun observeSchoolDataResult() {
        viewModel.satScoreResult.observe(this@SchoolInfoAndSATScoreActivity) {
            when (it) {
                is BaseResponse.Loading -> {}
                is BaseResponse.Success -> {
                    if (it.data?.size == 0) {
                        binding.satBodyCL.visibility = View.GONE
                        binding.satNoResultBodyCL.visibility = View.VISIBLE
                    } else {
                        binding.satScores = it.data?.get(0)
                        binding.satBodyCL.visibility = View.VISIBLE
                        binding.satNoResultBodyCL.visibility = View.GONE
                    }
                }
                is BaseResponse.Error -> {
                    Log.e("", "Data from Server: ${it.msg}")
                    Toast.makeText(this, "SAT data loading failed! ${it.msg}", Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {
                    Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
}