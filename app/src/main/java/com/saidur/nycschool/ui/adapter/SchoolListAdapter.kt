package com.saidur.nycschool.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.saidur.nycschool.R
import com.saidur.nycschool.model.SchoolInfo


class SchoolListAdapter(
    private val schoolInfo: ArrayList<SchoolInfo>,
    aSchoolClickListener: ISchoolClickListener
) :
    RecyclerView.Adapter<SchoolListAdapter.ViewHolder>() {

    private var context: Context? = null
    private var listener: ISchoolClickListener? = null

    init {
        listener = aSchoolClickListener
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val schoolName: TextView
        val totalStudents: TextView
        val primaryAddressLine: TextView
        val addressLine2: TextView

        init {
            // Define click listener for the ViewHolder's View
            schoolName = view.findViewById(R.id.schoolName)
            totalStudents = view.findViewById(R.id.totalStudents)
            primaryAddressLine = view.findViewById(R.id.primaryAddressLine)
            addressLine2 = view.findViewById(R.id.addressLine2)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.school_item, viewGroup, false)
        context = viewGroup.context
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get element from the school data
        // contents of the view with that element
        viewHolder.schoolName.text = schoolInfo[position].schoolName
        viewHolder.totalStudents.text = schoolInfo[position].totalStudents
        viewHolder.primaryAddressLine.text = schoolInfo[position].primaryAddressLine1
        viewHolder.addressLine2.text = buildString {
            append(schoolInfo[position].city)
            append(" , ")
            append(schoolInfo[position].stateCode)
            append(" , ")
            append(schoolInfo[position].zip)
        }

        viewHolder.itemView.setOnClickListener {
            val installedApp: SchoolInfo = schoolInfo[position]
            try {
                listener?.click(installedApp)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    // Return the size of the school data (invoked by the layout manager)
    override fun getItemCount() = schoolInfo.size

    interface ISchoolClickListener {
        fun click(schoolDetail: SchoolInfo?)
    }
}
