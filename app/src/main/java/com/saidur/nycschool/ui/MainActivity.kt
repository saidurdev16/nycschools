package com.saidur.nycschool.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.saidur.nycschool.databinding.ActivityMainBinding
import com.saidur.nycschool.model.BaseResponse
import com.saidur.nycschool.model.SchoolInfo
import com.saidur.nycschool.ui.adapter.SchoolListAdapter
import com.saidur.nycschool.ui.viewmodels.MainActivityViewModel
import com.saidur.nycschool.utils.Constants.SCHOOL_INFO
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), SchoolListAdapter.ISchoolClickListener {
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // obtain view-model instance
        viewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]

        // this creates a vertical layout Manager
        binding.rlSchoolList.layoutManager = LinearLayoutManager(this)
        // fetch school data from server
        viewModel.getSchoolInfo()
        // observe server response
        observeSchoolDataResult()
    }

    private fun observeSchoolDataResult() {
        viewModel.schoolInfoResult.observe(this@MainActivity) {
            when (it) {
                is BaseResponse.Loading -> {
                    binding.prgBarSchool.visibility = View.VISIBLE
                }

                is BaseResponse.Success -> {
                    binding.prgBarSchool.visibility = View.GONE
                    binding.rlSchoolList.adapter =
                        SchoolListAdapter(it.data as ArrayList<SchoolInfo>, this)
                }

                is BaseResponse.Error -> {
                    binding.prgBarSchool.visibility = View.GONE
                    Log.e("", "Data from Server: ${it.msg}")
                    Toast.makeText(
                        this,
                        "School data loading failed! ${it.msg}",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                else -> {
                    binding.prgBarSchool.visibility = View.GONE
                    Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    override fun click(schoolDetail: SchoolInfo?) {
        startActivity(Intent(this, SchoolInfoAndSATScoreActivity::class.java).apply {
            // you can add values(if any) to pass to the next class or avoid using `.apply`
            if (schoolDetail != null) {
                putExtra(SCHOOL_INFO, schoolDetail)
            }
        })
    }
}