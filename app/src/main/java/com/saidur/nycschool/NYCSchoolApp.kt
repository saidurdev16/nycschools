package com.saidur.nycschool

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolApp : Application() {}