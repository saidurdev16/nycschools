package com.saidur.nycschool.api

import com.saidur.nycschool.model.SchoolSatScore
import com.saidur.nycschool.model.SchoolInfo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolWebDataApi {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolList(): Response<List<SchoolInfo>>

    @GET("f9bf-2cp4.json")
    suspend fun getSatScoreDetail(@Query("dbn") dbn: String): Response<List<SchoolSatScore>>

}