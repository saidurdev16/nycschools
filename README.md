#20231014-MuhammadRahman-NYCSchools

## Getting started
A native Android app to provide information on NYC High schools.

## Implemented features
- ✨ Used Kotlin Language
- ✨ Used MVVM design pattern
- ✨ Used Hilt for Dependency injection
- ✨ Used Coroutine for network call
- ✨ Used recyclerview for presenting the list of schools
- ✨ Used view binding and data binding feature
- ✨ Organized files in a proper package structure
- ✨ Used retrofit for the network call
- ✨ Used sealed class for api response
- ✨ Used scope functions
- ✨ Used LiveData to observe result from Activity